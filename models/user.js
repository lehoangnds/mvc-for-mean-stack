var mongoose = require('mongoose');

UserSchema = mongoose.Schema({
	fullname: {
		type: String,
		required: true,
	},
   	username: {
		type: String,
		required: true,
		minlenght: [5, 'Username must be 5 characters or more.']
	},
	password: {
		type: String,
		required: true,
		minlenght: [8, 'Username must be 5 characters or more.']
	},
	created_at: {
		type: Date,
		default: ""
	},
	updated_at: {
		type: Date,
		default: ""
	}
});

/*
 * Export userSchema với User là collection tự động tạo collection users trong mongodb
 */
module.exports = mongoose.model('User', UserSchema);
