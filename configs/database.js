var mongoose = require('mongoose');

module.exports = function(){
	var dbName = "test";
	mongoose.Promise = global.Promise;
	mongoose.connect('mongodb://localhost/' + dbName);
	mongoose.connection
			.once('open', function(){
				console.log("* Da ket noi voi database: " + dbName);
			})
			.on('error', function(err){
				console.error("* Loi...Khong the ket noi den database.");
			});
}



