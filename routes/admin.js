var express     = require('express'),
    router      = express.Router();

/*
 * Sử dụng module multer để upload file
 */
var multer  = require('multer'),
    upload  = multer({dest: 'app/uploads/news/'}),
    fs      = require('fs');

/*
 * chèn controller
 */
var  Admin = require('../controllers/admin.js');
var  User  = require('../controllers/users.js');

/*
 * gọi đến function showAdminPage() trong controller admin.js
 */
router.get('/', Admin.showAdminPage)
		
      .get('/admin/listUser', User.getAllUsers)

module.exports = router;