var express     = require('express'),
    router      = express.Router();

/*
 * chèn controller
 */
var  Admin = require('../controllers/index.js');

/*
 * gọi đến function showIndexPage() trong controller index.js
 */
router.get('/', Admin.showIndexPage)

module.exports = router;