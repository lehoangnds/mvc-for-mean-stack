var app = angular.module('myApp', []);

app.controller('myController', ['$scope',  '$http', function($scope,  $http){
	/*
	 * $scope là biến liên kết giữa cotroller của angular vớ view
	 */
	$scope.hello = "Wellcome to MEAN stack of nodejs . Demo by Hoàng kun";
}]);