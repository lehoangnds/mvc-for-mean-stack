/*
 *	khai báo các package
 */
const   express 	 = require('express'),
		path		 = require('path'),
		multer		 = require('multer'),
		morgan		 = require('morgan'),
		bodyParser   = require('body-parser'),
		cookieParser = require('cookie-parser');


/*
 *	khai báo các hằng
 */
const port 		 = process.env.PORT || 3000;
const app		 = express();
const server 	 = require('http').Server(app);

/*
 *	Sử dụng middlewares
 */
app.use(bodyParser.urlencoded({extended : false}));
app.use(bodyParser.json());

/*
 *	Chèn database
 */
const databaseConfig = require('./configs/database');
	// khởi tạo database
databaseConfig();

/*
 *	sử dụng folder mặc định là public
 */
app.use(express.static(path.join(__dirname, '/public')));

/*
 *	sử dụng cookieParser
 */
app.use(cookieParser());

/*
 *	Gán thư mục views mặc định và sử dụng view engine là pug
 */
app.set('views', path.join(__dirname +'/views'));
app.set('view engine', 'pug');
app.locals.pretty = true;

/*
 *	Sử dụng router
 */
app.use('/', require('./routes/index.js'));
app.use('/admin', require('./routes/admin.js'));


/*
 *	Khai báo port
 */ 
server.listen(port , function(err){
	if(err){
		console.log(err);
	}else{
		console.log("* Ket noi den server thanh cong. Port la: " + port);
	}
});
