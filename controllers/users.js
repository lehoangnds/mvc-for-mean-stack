var userSchema = require('../models/user.js');

exports.getAllUsers = function(req, res){
	userSchema.find().exec(function(err,data){
		if(err){
			console.log(err);
		}else{
			res.json(data);
		}
	});
}