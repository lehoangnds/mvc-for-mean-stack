# Author: Lê Hoàng  #



### Cấu trúc thư mục theo mô hình MVC trong mean stack ###

# Khởi tạo: #
- Bước 1: Nhận cấu Project: git clone https://lehoangnds@bitbucket.org/lehoangnds/mvc-for-mean-stack.git

 + Kết quả sẽ tạo ra thư mục con có tên: mvc-for-mean-stack

- Bước 2: Vào project vừa nhận được: cd mvc-for-mean-stack

- Bước 3: Cài đặt các package: npm install

- Bước 4: Chạy project: npm start( không được thì dùng: npm run start hoặc node server)
** chú ý: nếu thực hiện các bước trên mà ko chạy được thì vào file server.js sửa lại port nhé :)

# Giải thích cấu trúc thư mục #

* file server.js : Cấu hình server

* file package.json : Chứa các package cần dùng: khi làm bước 3 ở trên npm install nó sẽ chạy file này

* Thư mục configs : Chứa các file cấu hình mặc định như, database, middlewares

* Thư mục controllers: Chứa các controller là thằng trung gian giữa model và view.

** Phụ trách lấy dữ liệu từ model và đổ ra view

* Thư mục models: Chứa các Schema


* Thư mục public: Chứa các file cần dùng trong view

** Trong đó thư mục css:  Chứa các file stylesheet

** Thư mục images: Chứa các file ảnh

** Thư mục js: Chứa các file js từ client, như các module của angular

** Thư mục libs: Chứa các thư viện cần dùng: như angular, jquery, slick .v.v.v

** Thư mục uploads: Nơi chưa các file upload, khi upload từ admin

* Thư mục views: Nơi chứa các file html

** Thư mục admin: Chứa các file html của admin

** Thư mục home: Chứa các file html của index

* file .babelrc : nơi khai báo preset (nếu sử dụng ES6 thì mới cần)